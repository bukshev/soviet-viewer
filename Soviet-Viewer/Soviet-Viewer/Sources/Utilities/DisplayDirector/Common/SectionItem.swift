//
//  SectionItem.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19.02.18.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import IGListKit

enum SectionItemCellIdentifier {
    case none
    case storyboardIdentifier(String)
    case cellType(UICollectionViewCell.Type)
    case nibName(String)
}

enum SectionItemCellSize {
    case none
    case auto
    case size(CGFloat, CGFloat)
}

protocol SectionItem {
    var cellIdentifier: SectionItemCellIdentifier { get }
    var cellSize: SectionItemCellSize { get }
}

typealias SectionItemAction = (ActionableSectionItem, Any?) -> Void

protocol ActionableSectionItem: SectionItem {
    var action: SectionItemAction? { get }
}

protocol DiffableSectionItem {
    var diffToken: String { get }

    func diff() -> ListDiffable
}

extension DiffableSectionItem {
    func diff() -> ListDiffable {
        return SectionItemDiff(diffToken: diffToken)
    }
}

fileprivate class SectionItemDiff: ListDiffable {
    let diffToken: String

    init(diffToken: String) {
        self.diffToken = diffToken
    }

    public func diffIdentifier() -> NSObjectProtocol {
        return diffToken as NSObjectProtocol
    }

    public func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard let object = object as? SectionItemDiff else { return false }
        return diffToken == object.diffToken
    }
}
