//
//  DashboardInteractorIO.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 20.02.18.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import Foundation

protocol DashboardInteractorInput: class {
    func loadNewPosts(page: Int)
}

protocol DashboardInteractorOutput: class {
    func didLoad(newPosts: [Post])
    func didFailureLoadNewPosts(error: SovietError)
}
