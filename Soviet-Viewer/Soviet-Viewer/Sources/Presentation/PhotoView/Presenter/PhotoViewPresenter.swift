//
//  PhotoViewPresenter.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 20/02/2018.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import Foundation

class PhotoViewPresenter {
    weak var view: PhotoViewInput!
    var router: PhotoViewRouterInput!
    weak var moduleOutput: PhotoViewModuleOutput?

    fileprivate var post: Post?
    fileprivate var postIndex = -1
}

extension PhotoViewPresenter: PhotoViewModuleInput {
    func set(moduleOutput: PhotoViewModuleOutput) {
        self.moduleOutput = moduleOutput
    }

    func set(post: Post, index: Int) {
        self.post = post
        self.postIndex = index
    }
}

extension PhotoViewPresenter: PhotoViewOutput {
    func triggerViewReadyEvent() {
        view.setupState(with: post)
    }

    func triggerCloseEvent() {
        router.close()
    }

    func triggerShowPreviousImage() {
        guard let moduleOutput = moduleOutput else { return }
        post = moduleOutput.post(before: postIndex)
        postIndex = (postIndex - 1 >= 0) ? (postIndex - 1) : 0

        guard let post = post else { return }
        view.setupState(with: post)
    }

    func triggerShowNextImage() {
        guard let moduleOutput = moduleOutput else { return }
        post = moduleOutput.post(after: postIndex)
        postIndex = postIndex + 1

        guard let post = post else { return }
        view.setupState(with: post)
    }
}
