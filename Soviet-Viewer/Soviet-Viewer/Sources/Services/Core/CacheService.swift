//
//  CacheService.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19.02.18.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import Foundation
import RealmSwift

protocol CacheServiceProtocol {
    func has(type: Object.Type) -> Bool
    func objects<T: Object>() -> [T]
    func cache<T: Object>(objects: [T])
    func removeAll()
}

class CacheService {
    let realm = try! Realm()
}

extension CacheService: CacheServiceProtocol {
    func has(type: Object.Type) -> Bool {
        return !realm.objects(type).isEmpty
    }

    func objects<T>() -> [T] where T : Object {
        return Array(realm.objects(T.self))
    }

    func cache<T>(objects: [T]) where T : Object {
        DispatchQueue.main.async {
            do {
                try self.realm.write {
                    objects.forEach { self.realm.create(T.self, value: $0, update: false) }
                }
            } catch {
                debugPrint("Can't cache objects in Realm \(#function)")
            }
        }
    }

    func removeAll() {
        DispatchQueue.main.async {
            do {
                try self.realm.write { self.realm.deleteAll() }
            } catch {
                debugPrint("Can't remove objects from Realm \(#function)")
            }
        }
    }
}
