//
//  DisplayDirectorSource.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19.02.18.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import IGListKit

class DisplayDirectorSource {
    fileprivate let uuid = UUID().uuidString

    let data: Any
    let type: ListSectionController.Type

    init(data: Any, type: ListSectionController.Type) {
        self.data = data
        self.type = type
    }
}

extension DisplayDirectorSource: ListDiffable {
    public func diffIdentifier() -> NSObjectProtocol {
        return uuid as NSObjectProtocol
    }

    public func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard let object = object as? DisplayDirectorSource else { return false }
        return uuid == object.uuid
    }
}
