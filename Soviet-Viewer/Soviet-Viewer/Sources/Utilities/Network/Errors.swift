//
//  Errors.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19.02.18.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import Foundation

enum SovietError: Error {
    // Networking
    case network(description: String)
    case api(description: String, json: JSONType?)
    case invalidType(expect: Any.Type, got: Any.Type)
    case invalidToken
    case validation([String])

    // Cache
    case cacheNotFound(Any.Type)

    // Mapping
    case failureMap(MappableProtocol.Type)

    // Other
    case unknown(String?)
}
