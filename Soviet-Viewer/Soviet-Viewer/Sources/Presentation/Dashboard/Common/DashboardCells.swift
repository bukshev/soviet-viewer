//
//  DashboardCells.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19.02.18.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import SDWebImage

class DashboardPostCell: BaseCollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!

    override func configure(with item: SectionItem) {
        super.configure(with: item)
        guard let item = item as? DashboardPostItem else { return }

        let placeholderImage = UIImage(named: item.placeholderImageName)
        if let imageUrl = item.post.imageURL() {
            imageView.sd_setImage(with: imageUrl, placeholderImage: placeholderImage) { (image, error, _, _) in
                guard let image = image else { return }
                item.didLoad(imageData: image.sd_imageData(as: .JPEG))
            }
        } else {
            imageView.image = placeholderImage
        }
    }

    override func preferredLayoutAttributesFitting(_ layoutAttributes: CellLayoutAttributes) -> CellLayoutAttributes {
        guard let image = imageView.image else { return layoutAttributes }

        var frame = layoutAttributes.frame
        let ratio = image.size.width / UIScreen.main.bounds.width
        frame.size.width = image.size.width / ratio
        frame.size.height = image.size.height / ratio
        layoutAttributes.frame = frame
        return layoutAttributes
    }
}

class DashboardEmptyViewCell: BaseCollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!

    override func preferredLayoutAttributesFitting(_ layoutAttributes: CellLayoutAttributes) -> CellLayoutAttributes {
        layoutAttributes.frame = UIScreen.main.bounds
        return layoutAttributes
    }
}
