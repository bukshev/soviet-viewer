//
//  SplashPresenter.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19/02/2018.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import Foundation

class SplashPresenter {
    weak var view: (SplashViewInput & ProgressIndication)!
    var interactor: SplashInteractorInput!
    var router: SplashRouterInput!

    fileprivate let hintsStore = SplashHintsStore()
}

extension SplashPresenter: SplashViewOutput {
    func triggerViewReadyEvent() {
        view.setupInitialState(hint: hintsStore.randomHint())
        view.showProgressHUD()
        view.showNetworkActivity()
    }

    func triggerViewDidAppearEvent() {
        interactor.obtainPosts()
    }
}

extension SplashPresenter: SplashInteractorOutput {
    func didObtain(posts: [Post]) {
        view.hideProgressHUD()
        view.hideNetworkActivity()
        router.openDashboardScreen(posts: posts)
    }

    func didFailureObtainPosts(error: SovietError) {
        view.hideProgressHUD()
        view.hideNetworkActivity()
    }
}
