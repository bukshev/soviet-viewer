//
//  DashboardRouter.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19/02/2018.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import Foundation

class DashboardRouter {
    weak var transitionHandler: ModuleTransitionHandlerProtocol!

    var photoViewModuleFactory: ModuleFactory<PhotoViewController>!
}

extension DashboardRouter: DashboardRouterInput {
    func openPhotoView(post: Post, index: Int, moduleOutput: PhotoViewModuleOutput) {
        let module = transitionHandler.openModule(factory: photoViewModuleFactory)
        let configuredModule = module.configure { (input: PhotoViewModuleInput) in
            input.set(moduleOutput: moduleOutput)
            input.set(post: post, index: index)
        }
        configuredModule.transition(type: .present(type: .modal))
    }
}
