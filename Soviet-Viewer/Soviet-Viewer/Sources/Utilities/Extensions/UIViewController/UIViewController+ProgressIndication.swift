//
//  UIViewController+ProgressIndication.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19.02.18.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import MBProgressHUD

protocol ProgressIndication: class {
    func showProgressHUD()
    func showProgressHUD(message: String)
    func hideProgressHUD()
}

extension UIViewController: ProgressIndication {
    func showProgressHUD() {
        showProgressHUD(message: "")
    }

    func showProgressHUD(message: String) {
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.label.text = message
        hud.removeFromSuperViewOnHide = true
    }

    func hideProgressHUD() {
        MBProgressHUD.hide(for: view, animated: true)
    }
}
