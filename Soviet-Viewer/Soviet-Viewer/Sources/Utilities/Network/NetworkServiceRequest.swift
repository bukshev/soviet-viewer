//
//  NetworkServiceRequest.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19.02.18.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import Alamofire

typealias JSONType = [String: Any]
typealias JSONArrayType = [JSONType]

struct NetworkServiceRequest {
    enum HTTPMethod {
        case get
        case post
        case put
    }

    enum JSONParameters {
        case dictionary(JSONType)
        case array(JSONArrayType)
    }

    enum Encoding {
        case methodDependent
        case queryString
        case httpBody
        case JSON
    }

    var url = URL(string: "http://www.google.com")!
    var method: HTTPMethod = .get
    var parameters: JSONParameters?
    var headers: [String: String]?
    var encoding: Encoding = .methodDependent
}

extension NetworkServiceRequest: Equatable {
    public static func == (lhs: NetworkServiceRequest, rhs: NetworkServiceRequest) -> Bool {
        return ((lhs.url == rhs.url) && (lhs.method == rhs.method))
    }
}
