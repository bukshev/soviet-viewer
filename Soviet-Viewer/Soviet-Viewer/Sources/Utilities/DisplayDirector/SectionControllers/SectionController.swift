//
//  SectionController.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19.02.18.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import IGListKit

protocol SectionItemCell: class {
    func configure(with item: SectionItem)
    func deconfigure(with item: SectionItem)
}

class SectionController: ListSectionController {
    var items: [SectionItem] = []

    func setup() {
        displayDelegate = self
    }

    override func numberOfItems() -> Int {
        return items.count
    }

    override func sizeForItem(at index: Int) -> CGSize {
        return cellSize(for: items[index])
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        let item = items[index]
        let cell = dequeueReusableCell(for: item, at: index)
        configure(cell: cell, item: item)
        return cell
    }

    override func didUpdate(to object: Any) {
        if let object = object as? DisplayDirectorSource, let items = object.data as? [SectionItem] {
            self.items = items
        }
    }

    override func didSelectItem(at index: Int) {
        if let item = items[index] as? ActionableSectionItem, let action = item.action {
            action(item, self)
        }
    }
}

// swiftlint:disable line_length
extension SectionController: ListDisplayDelegate {
    public func listAdapter(_ listAdapter: ListAdapter, willDisplay sectionController: ListSectionController) {

    }

    public func listAdapter(_ listAdapter: ListAdapter, didEndDisplaying sectionController: ListSectionController) {

    }

    public func listAdapter(_ listAdapter: ListAdapter, willDisplay sectionController: ListSectionController, cell: UICollectionViewCell, at index: Int) {

    }

    public func listAdapter(_ listAdapter: ListAdapter, didEndDisplaying sectionController: ListSectionController, cell: UICollectionViewCell, at index: Int) {
        if sectionController.isEqual(self) && items.indices.contains(index) {
            deconfigure(cell: cell, item: items[index])
        }
    }
}
// swiftlint:enable line_length

extension SectionController {
    var collectionViewUsedAutoSizing: Bool {
        guard let adapter = collectionContext as? ListAdapter else { return false }
        guard let layout = adapter.collectionView?.collectionViewLayout as? UICollectionViewFlowLayout else { return false }
        return layout.estimatedItemSize != .zero
    }

    func dequeueReusableCell(for item: SectionItem, at index: Int) -> UICollectionViewCell {
        let cell: UICollectionViewCell

        switch item.cellIdentifier {
        case .none:
            fatalError()
        case let .cellType(CellType):
            cell = collectionContext!.dequeueReusableCell(of: CellType, for: self, at: index)
        case let .storyboardIdentifier(identifier):
            cell = collectionContext!.dequeueReusableCellFromStoryboard(withIdentifier: identifier, for: self, at: index)
        case let .nibName(name):
            cell = collectionContext!.dequeueReusableCell(withNibName: name, bundle: nil, for: self, at: index)
        }
        return cell
    }

    func configure(cell: UICollectionViewCell, item: SectionItem) {
        if let cell = cell as? SectionItemCell {
            cell.configure(with: item)
        }
    }

    func deconfigure(cell: UICollectionViewCell, item: SectionItem) {
        if let cell = cell as? SectionItemCell {
            cell.deconfigure(with: item)
        }
    }

    func cellSize(for item: SectionItem) -> CGSize {
        switch item.cellSize {
        case let .size(width, height):
            return CGSize(width: width, height: height)
        case .auto:
            if let cell = DisplayDirectorCellCache.shared.cell(for: item) {
                configure(cell: cell, item: item)
                if let size = collectionContext?.containerSize {
                    var attributes = CellLayoutAttributes()
                    attributes.frame = CGRect(origin: .zero, size: size)
                    attributes = cell.preferredLayoutAttributesFitting(attributes)
                    return attributes.frame.size
                }
            }
        default:
            break
        }
        return CGSize(width: collectionContext!.containerSize.width, height: 55)
    }
}

extension SectionController: SectionContext {
    func scroll(at index: Int, scrollPosition: UICollectionViewScrollPosition, animated: Bool) {
        collectionContext?.scroll(to: self, at: index, scrollPosition: scrollPosition, animated: animated)
    }

    func performBatch(animated: Bool, updates: @escaping (SectionBatchContext) -> Void) {
        collectionContext?.performBatch(animated: animated, updates: { context in
            let batchContext = SectionControllerBatchContext(sectionController: self, context: context)
            updates(batchContext)
        }, completion: nil)
    }

    func reload() {
        collectionContext?.performBatch(animated: true, updates: { context in
            context.reload(self)
        }, completion: nil)
    }
}

fileprivate struct SectionControllerBatchContext: SectionBatchContext {
    let sectionController: SectionController
    let context: ListBatchContext

    func reload(at indexes: IndexSet) {
        context.reload(in: sectionController, at: indexes)
    }

    func insert(at indexes: IndexSet) {
        context.insert(in: sectionController, at: indexes)
    }

    func delete(at indexes: IndexSet) {
        context.delete(in: sectionController, at: indexes)
    }

    func move(from fromIndex: Int, to toIndex: Int) {
        context.move(in: sectionController, from: fromIndex, to: toIndex)
    }
}
