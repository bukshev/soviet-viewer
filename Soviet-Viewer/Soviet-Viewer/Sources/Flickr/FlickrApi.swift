//
//  FlickrApi.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 20.02.18.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import Foundation

struct FlickrApi {
    static let apiKey = "17de0f15137f89b53382039c8952d27e"
    static let endpoint = "https://api.flickr.com/services/rest/"
    static let format = "json"

    static func imageUrl(post: Post, sizeSuffix: String? = nil, imageFormat: String = ".jpg") -> URL? {
        var urlString = "https://farm\(post.farm).staticflickr.com/\(post.server)/\(post.identifier)_\(post.secret)"
        if let sizeSuffix = sizeSuffix {
            urlString += "_[\(sizeSuffix)]"
        }
        urlString += imageFormat

        return URL(string: urlString)
    }
}

extension FlickrApi {
    /// GET "flickr.photos.search"
    static func getImagesRequest(hashtag: String, page: Int) -> NetworkServiceRequest {
        var request = NetworkServiceRequest()
        request.url = URL(string: endpoint)!
        request.method = .get
        request.parameters = .dictionary([
            "method": "flickr.photos.search",
            "api_key": apiKey,
            "text": hashtag,
            "page": page,
            "format": format,
            "nojsoncallback": 1
        ])
        return request
    }
}
