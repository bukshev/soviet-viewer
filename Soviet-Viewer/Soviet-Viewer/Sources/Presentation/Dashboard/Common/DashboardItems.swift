//
//  DashboardItems.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19.02.18.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import Foundation

class DashboardPostItem: ActionableSectionItem {
    let cellIdentifier: SectionItemCellIdentifier = .nibName("DashboardPostCell")
    var cellSize: SectionItemCellSize = .auto
    var action: SectionItemAction?

    var post: Post
    let placeholderImageName: String

    init(post: Post, placeholderImageName: String) {
        self.post = post
        self.placeholderImageName = placeholderImageName
    }

    func didLoad(imageData: Data?) {
        guard let data = imageData else { return }
        post.update(imageData: data)
    }
}

class DashboardEmptyViewItem: SectionItem {
    let cellIdentifier: SectionItemCellIdentifier = .nibName("DashboardEmptyViewCell")
    var cellSize: SectionItemCellSize = .auto

    let imageName: String

    init(imageName: String) {
        self.imageName = imageName
    }
}
