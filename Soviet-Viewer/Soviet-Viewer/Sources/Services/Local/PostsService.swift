//
//  PostsService.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19.02.18.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import Foundation

protocol PostsServiceProtocol {
    func loadPosts(hashtag: String, page: Int, handler: @escaping (Result<[Post]>) -> Void)
}

class PostsService {
    var networkService: NetworkServiceProtocol!
    var objectMapperService: ObjectMapperServiceProtocol!
}

extension PostsService: PostsServiceProtocol {
    func loadPosts(hashtag: String, page: Int, handler: @escaping (Result<[Post]>) -> Void) {
        postsSignal(hashtag: hashtag, page: page).subscribe { (result) in handler(result) }
    }
}

fileprivate extension PostsService {
    func postsSignal(hashtag: String, page: Int) -> Signal<[Post]> {
        let request = FlickrApi.getImagesRequest(hashtag: hashtag, page: page)
        return networkService.request(request: request).flatMap { (value: JSONType) -> Signal<[Post]> in
            if let embedded = value["photos"] as? JSONType, let photos = embedded["photo"] as? JSONArrayType {
                return self.objectMapperService.mapArray(JSON: photos).toSignal()
            }
            return Signal(failure: .failureMap(Post.self))
        }
    }
}
