//
//  SplashInteractor.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19/02/2018.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import Foundation

class SplashInteractor {
    weak var output: SplashInteractorOutput!

    var reachabilityService: ReachabilityServiceProtocol!
    var postsService: PostsServiceProtocol!
    var cacheService: CacheServiceProtocol!
}

extension SplashInteractor: SplashInteractorInput {
    func obtainPosts() {
        guard reachabilityService.hasInternerConnection else {
            let posts: [Post] = cacheService.objects()
            output.didObtain(posts: posts)
            return
        }

        cacheService.removeAll()
        postsService.loadPosts(hashtag: "chow-chow", page: 1) { [unowned self] (result) in
            switch result {
            case let .success(posts):
                self.cacheService.cache(objects: posts)
                self.output.didObtain(posts: posts)
            case let .failure(error):
                self.output.didFailureObtainPosts(error: error)
            }
        }
    }
}
