//
//  DisplayDirector.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19.02.18.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import IGListKit

final class DisplayDirector {
    fileprivate let dataSource = DisplayDirectorListAdapterDataSource()
    fileprivate var adapter = ListAdapter(updater: ListAdapterUpdater(), viewController: nil, workingRangeSize: 0)

    fileprivate var sources: [DisplayDirectorSource] = []
    var emptyView: UIView?

    weak var scrollViewDelegate: UIScrollViewDelegate? {
        get { return adapter.scrollViewDelegate }
        set { adapter.scrollViewDelegate = newValue }
    }

    init(collectionView: UICollectionView) {
        collectionView.isPrefetchingEnabled = false

        dataSource.displayDirector = self
        adapter.collectionView = collectionView
        adapter.dataSource = dataSource
    }

    func setSources(_ sources: [DisplayDirectorSource], completion: ((Bool) -> Void)? = nil) {
        self.sources = sources
        reloadData(completion: completion)
    }

    func performUpdates(animated: Bool = true, completion: ((Bool) -> Void)? = nil) {
        adapter.performUpdates(animated: animated) { finished in
            completion?(finished)
        }
    }

    func reloadData(completion: ((Bool) -> Void)? = nil) {
        adapter.reloadData { finished in
            completion?(finished)
        }
    }

    func sectionController(at index: Int) -> ListSectionController? {
        guard sources.indices.contains(index) else { return nil }
        return adapter.sectionController(for: sources[index])
    }
}

fileprivate extension DisplayDirector {
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        return sources
    }

    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        guard let object = object as? DisplayDirectorSource else { return ListSectionController() }

        let sectionController = object.type.init()
        if let sectionController = sectionController as? SectionController {
            sectionController.setup()
        }
        return sectionController
    }

    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        return emptyView
    }
}

fileprivate class DisplayDirectorListAdapterDataSource: NSObject, ListAdapterDataSource {
    weak var displayDirector: DisplayDirector!

    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        return displayDirector.objects(for: listAdapter)
    }

    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        return displayDirector.listAdapter(listAdapter, sectionControllerFor: object)
    }

    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        return displayDirector.emptyView(for: listAdapter)
    }
}
