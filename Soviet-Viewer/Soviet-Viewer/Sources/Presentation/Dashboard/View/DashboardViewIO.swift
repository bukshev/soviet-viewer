//
//  DashboardViewIO.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19/02/2018.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import Foundation

protocol DashboardViewInput: class {
    func setupInitialState()
}

protocol DashboardViewOutput: class {
    weak var displayDirector: DisplayDirector! { get set }
    var loading: Bool { get }

    func triggerViewReadyEvent()
    func triggerLoadMoreEvent()
}
