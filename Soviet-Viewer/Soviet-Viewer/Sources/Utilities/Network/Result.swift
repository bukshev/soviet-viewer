//
//  Result.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19.02.18.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import Foundation

enum Result<Type> {
    case success(Type)
    case failure(SovietError)

    var value: Type? {
        switch self {
        case let .success(value):
            return value
        default:
            return nil
        }
    }

    var error: SovietError? {
        switch self {
        case let .failure(error):
            return error
        default:
            return nil
        }
    }
}

extension Result {
    func toSignal() -> Signal<Type> {
        switch self {
        case let .success(value):
            return Signal(success: value)
        case let .failure(error):
            return Signal(failure: error)
        }
    }
}
