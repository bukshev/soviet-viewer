//
//  Colors.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19.02.18.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import UIKit

enum Colors {
    case white
    case paperBackground
    case bostonRed
    case schoolBusYellow
}

extension Colors {
    var color: UIColor {
        var instanceColor = UIColor.clear

        switch self {
        case .white:
            instanceColor = UIColor.white
        case .paperBackground:
            instanceColor = UIColor(hexString: "#EFEFEF")
        case .bostonRed:
            instanceColor = UIColor(hexString: "#BC0707")
        case .schoolBusYellow:
            instanceColor = UIColor(hexString: "#FFD900")

        }

        return instanceColor
    }
}
