//
//  SplashViewIO.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19/02/2018.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import Foundation

protocol SplashViewInput: class {
    func setupInitialState(hint: String)

    func showNetworkActivity()
    func hideNetworkActivity()
}

protocol SplashViewOutput: class {
    func triggerViewReadyEvent()
    func triggerViewDidAppearEvent()
}
