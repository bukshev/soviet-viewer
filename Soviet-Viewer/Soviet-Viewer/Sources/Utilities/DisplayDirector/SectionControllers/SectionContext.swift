//
//  SectionContext.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19.02.18.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import UIKit

protocol SectionBatchContext {
    func reload(at indexes: IndexSet)
    func insert(at indexes: IndexSet)
    func delete(at indexes: IndexSet)
    func move(from fromIndex: Int, to toIndex: Int)
}

protocol SectionContext: class {
    var items: [SectionItem] { get set }

    func scroll(at index: Int, scrollPosition: UICollectionViewScrollPosition, animated: Bool)
    func performBatch(animated: Bool, updates: @escaping (SectionBatchContext) -> Void)
    func reload()
}
