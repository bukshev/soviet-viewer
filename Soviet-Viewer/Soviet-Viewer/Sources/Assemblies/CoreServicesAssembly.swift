//
//  CoreServicesAssembly.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19.02.18.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import Swinject

class CoreServiceComponents: Assembly {
    func assemble(container: Container) {
        container.register(NetworkServiceProtocol.self) { _ in NetworkService() }

        container.register(CacheServiceProtocol.self) { _ in CacheService() }
            .inObjectScope(.container)

        container.register(ObjectMapperServiceProtocol.self) { _ in ObjectMapperService() }
            .inObjectScope(.container)

        container.register(ReachabilityServiceProtocol.self) { _ in ReachabilityService() }
            .inObjectScope(.container)
    }
}
