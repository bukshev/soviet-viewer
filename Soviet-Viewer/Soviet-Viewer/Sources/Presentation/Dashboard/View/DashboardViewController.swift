//
//  DashboardViewController.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19/02/2018.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import UIKit

class DashboardViewController: UIViewController {
    // MARK: Outlets
    @IBOutlet weak var collectionView: UICollectionView!

    var output: DashboardViewOutput!
    fileprivate var displayDirector: DisplayDirector!
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.triggerViewReadyEvent()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let navController = navigationController else { return }
        navController.navigationBar.barTintColor = Colors.bostonRed.color
        navController.navigationBar.tintColor = Colors.white.color
        navController.navigationBar.titleTextAttributes = [
            NSAttributedStringKey.foregroundColor: Colors.white.color
        ]
    }
}

extension DashboardViewController: UIScrollViewDelegate {
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let distance = scrollView.contentSize.height - (targetContentOffset.pointee.y + scrollView.bounds.height)
        guard !output.loading && distance < (view.bounds.height / 2) else { return }
        output.triggerLoadMoreEvent()
    }
}

extension DashboardViewController: DashboardViewInput {
    func setupInitialState() {
        title = "Chow chow"
        view.backgroundColor = Colors.paperBackground.color
        collectionView.backgroundColor = Colors.paperBackground.color
        displayDirector = DisplayDirector(collectionView: collectionView)
        displayDirector.scrollViewDelegate = self
        output.displayDirector = displayDirector
    }
}

extension DashboardViewController: ModuleView {}
