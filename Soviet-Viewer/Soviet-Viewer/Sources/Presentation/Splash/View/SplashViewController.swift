//
//  SplashViewController.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19/02/2018.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {
    @IBOutlet weak var hintLabel: UILabel!

    override var prefersStatusBarHidden: Bool {
        return true
    }

    var output: SplashViewOutput!

    override func viewDidLoad() {
        super.viewDidLoad()
        output.triggerViewReadyEvent()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        output.triggerViewDidAppearEvent()
    }
}

extension SplashViewController: SplashViewInput {
    func setupInitialState(hint: String) {
        hintLabel.text = hint
        hintLabel.textColor = Colors.white.color
    }

    func showNetworkActivity() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }

    func hideNetworkActivity() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

extension SplashViewController: ModuleView {}
