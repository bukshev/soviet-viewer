//
//  SplashRouter.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19/02/2018.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import Foundation

class SplashRouter {
    weak var transitionHandler: ModuleTransitionHandlerProtocol!

    var dashboardModuleFactory: ModuleFactory<DashboardViewController>!
}

extension SplashRouter: SplashRouterInput {
    func openDashboardScreen(posts: [Post]) {
        let module = transitionHandler.openModule(factory: dashboardModuleFactory)
        let configuredModule = module.configure { (input: DashboardModuleInput) in
            input.set(posts: posts)
        }
        configuredModule.transition(type: .present(type: .embeded))
    }
}
