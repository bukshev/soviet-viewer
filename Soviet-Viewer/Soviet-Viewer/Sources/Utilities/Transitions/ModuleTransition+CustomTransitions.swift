//
//  ModuleTransition+CustomTransitions.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19.02.18.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import UIKit

enum NavigationEmbedMode {
    case modal
    case embeded
}

enum TransitionType {
    case root
    case push
    case present(type: NavigationEmbedMode)
    case custom(transition: (_ source: UIViewController, _ destination: UIViewController) -> Void)
}

extension NavigationEmbedMode {
    fileprivate func viewControllerForNavigationEmbedMode(_ viewController: UIViewController) -> UIViewController {
        switch self {
        case .modal:
            return viewController
        case .embeded:
            return UINavigationController(rootViewController: viewController)
        }
    }
}

extension TransitionType {
    fileprivate var window: UIWindow? {
        if let delegate = UIApplication.shared.delegate, let window = delegate.window {
            return window
        }
        return nil
    }

    fileprivate func perform(_ source: UIViewController, _ destination: UIViewController) {
        switch self {
        case .root:
            window?.rootViewController = destination
        case .push:
            source.navigationController?.pushViewController(destination, animated: true)
        case let .present(type):
            source.present(type.viewControllerForNavigationEmbedMode(destination), animated: true, completion: nil)
        case let .custom(transition):
            transition(source, destination)
        }
    }
}

extension ModuleTransitionProtocol {
    func transition(type: TransitionType) {
        transition { (source, destination) in
            type.perform(source, destination)
        }
    }
}
