//
//  LocalServicesAssembly.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19.02.18.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import Swinject

class LocalServicesAssembly: Assembly {
    func assemble(container: Container) {
        container.register(PostsServiceProtocol.self) { r in
            let c = PostsService()
            c.networkService = r.resolve(NetworkServiceProtocol.self)
            c.objectMapperService = r.resolve(ObjectMapperServiceProtocol.self)
            return c
        }
    }
}
