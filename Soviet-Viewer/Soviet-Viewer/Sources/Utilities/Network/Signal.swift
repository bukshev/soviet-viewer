//
//  Signal.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19.02.18.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import Foundation

fileprivate enum SignalType<Type> {
    case dynamic(action: ((SignalEvent<Type>) -> Void))
    case success(value: Type)
    case failure(error: SovietError)
}

class SignalEvent<Type> {
    fileprivate let subscriber: (Result<Type>) -> Void

    fileprivate init(subscriber: @escaping (Result<Type>) -> Void) {
        self.subscriber = subscriber
    }

    func success(value: Type) {
        subscriber(.success(value))
    }

    func failure(error: SovietError) {
        subscriber(.failure(error))
    }
}

class Signal<Type> {
    fileprivate let type: SignalType<Type>

    init(action: @escaping ((SignalEvent<Type>) -> Void)) {
        type = .dynamic(action: action)
    }

    init(success: Type) {
        type = .success(value: success)
    }

    init(failure: SovietError) {
        type = .failure(error: failure)
    }

    func subscribe(success: @escaping (Type) -> Void, failure: @escaping (SovietError) -> Void) {
        subscribe { result in
            switch result {
            case let .success(value):
                success(value)
            case let .failure(error):
                failure(error)
            }
        }
    }

    func subscribe(subscriber: @escaping (Result<Type>) -> Void) {
        switch type {
        case let .dynamic(action):
            action(SignalEvent(subscriber: subscriber))
        case let .success(value):
            subscriber(.success(value))
        case let .failure(error):
            subscriber(.failure(error))
        }
    }
}

extension Signal {
    func map<NewType>(_ transform: @escaping (Type) -> NewType) -> Signal<NewType> {
        return Signal<NewType> { event in
            self.subscribe(success: { event.success(value: transform($0)) }, failure: { event.failure(error: $0) })
        }
    }

    func flatMap<NewType>(with other: @escaping (Type) -> Signal<NewType>) -> Signal<NewType> {
        return Signal<NewType> { event in
            self.subscribe(success: {
                other($0).subscribe(success: { event.success(value: $0) }, failure: { event.failure(error: $0) })
            }, failure: {
                event.failure(error: $0)
            })
        }
    }

    func flatMapError(to other: @escaping (Error) -> Signal<Type>) -> Signal<Type> {
        return Signal { event in
            self.subscribe(success: {
                event.success(value: $0)
            }, failure: {
                other($0).subscribe(success: { event.success(value: $0) }, failure: { event.failure(error: $0) })
            })
        }
    }
}
