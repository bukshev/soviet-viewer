//
//  PhotoViewRouterIO.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 20/02/2018.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import Foundation

protocol PhotoViewRouterInput: class {
    func close()
}
