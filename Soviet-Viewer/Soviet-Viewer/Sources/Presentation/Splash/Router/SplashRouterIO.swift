//
//  SplashRouterIO.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19/02/2018.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import Foundation

protocol SplashRouterInput: class {
    func openDashboardScreen(posts: [Post])
}
