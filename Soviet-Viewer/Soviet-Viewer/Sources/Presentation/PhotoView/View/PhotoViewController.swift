//
//  PhotoViewController.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 20/02/2018.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import SDWebImage

class PhotoViewController: UIViewController {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!

    var output: PhotoViewOutput!

    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.triggerViewReadyEvent()
    }

    // MARK: User Actions
    @IBAction func touchCloseButton(_ sender: UIButton) {
        output.triggerCloseEvent()
    }

    @IBAction func swipeLeft(_ sender: UISwipeGestureRecognizer) {
        output.triggerShowNextImage()
    }

    @IBAction func swipeRight(_ sender: UISwipeGestureRecognizer) {
        output.triggerShowPreviousImage()
    }
}

extension PhotoViewController: PhotoViewInput {
    func setupState(with post: Post?) {
        guard let post = post else { return }
        imageView.sd_setImage(with: post.imageURL())
        titleLabel.text = post.title
        titleLabel.textColor = Colors.white.color
    }
}

extension PhotoViewController: ModuleView {}
