//
//  ModuleTransition.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19.02.18.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import UIKit

protocol ModuleTransitionProtocol {
    func transition(handler: (UIViewController, UIViewController) -> Void)
}

struct ModuleTransition: ModuleTransitionProtocol {
    let source: UIViewController
    let destination: UIViewController

    func transition(handler: (UIViewController, UIViewController) -> Void) {
        handler(source, destination)
    }
}
