//
//  PhotoViewModuleIO.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 20/02/2018.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import Foundation

protocol PhotoViewModuleInput: class {
    func set(moduleOutput: PhotoViewModuleOutput)
    func set(post: Post, index: Int)
}

protocol PhotoViewModuleOutput: class {
    func post(before index: Int) -> Post?
    func post(after index: Int) -> Post?
}
