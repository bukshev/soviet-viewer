//
//  ObjectMapperService.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19.02.18.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import ObjectMapper

protocol MappableProtocol: Mappable {

}

protocol MappaperContext: MapContext {

}

protocol ObjectMapperServiceProtocol {
    func map<Type: MappableProtocol>(JSON: JSONType?) -> Result<Type>
    func map<Type: MappableProtocol>(JSON: JSONType?, context: MappaperContext?) -> Result<Type>

    func mapArray<Type: MappableProtocol>(JSON: JSONArrayType?) -> Result<[Type]>
    func mapArray<Type: MappableProtocol>(JSON: JSONArrayType?, context: MappaperContext?) -> Result<[Type]>

    func json<Type: MappableProtocol>(object: Type) -> Result<JSONType>
    func jsonArray<Type: MappableProtocol>(objects: [Type]) -> Result<JSONArrayType>
}

class ObjectMapperService {

}

extension ObjectMapperService: ObjectMapperServiceProtocol {
    func map<Type: MappableProtocol>(JSON: JSONType?) -> Result<Type> {
        return map(JSON: JSON, context: nil)
    }

    func map<Type: MappableProtocol>(JSON: JSONType?, context: MappaperContext?) -> Result<Type> {
        if let JSON = JSON, let instanse = Type(JSON: JSON, context: context) {
            return .success(instanse)
        }
        return .failure(.failureMap(Type.self))
    }

    func mapArray<Type: MappableProtocol>(JSON: JSONArrayType?) -> Result<[Type]> {
        return mapArray(JSON: JSON, context: nil)
    }

    func mapArray<Type: MappableProtocol>(JSON: JSONArrayType?, context: MappaperContext?) -> Result<[Type]> {
        if let JSON = JSON {
            return .success([Type](JSONArray: JSON, context: context))
        }
        return .failure(.failureMap(Type.self))
    }

    func json<Type: MappableProtocol>(object: Type) -> Result<JSONType> {
        return .success(object.toJSON())
    }

    func jsonArray<Type: MappableProtocol>(objects: [Type]) -> Result<JSONArrayType> {
        return .success(objects.toJSON())
    }
}
