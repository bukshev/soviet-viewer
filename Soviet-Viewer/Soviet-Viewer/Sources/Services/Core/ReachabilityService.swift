//
//  ReachabilityService.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 20.02.18.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import Reachability

protocol ReachabilityServiceProtocol {
    var hasInternerConnection: Bool { get }
}

class ReachabilityService {
    let reachability = Reachability()
}

extension ReachabilityService: ReachabilityServiceProtocol {
    var hasInternerConnection: Bool {
        guard let reachability = reachability else { return false }
        return (reachability.connection != .none)
    }
}

