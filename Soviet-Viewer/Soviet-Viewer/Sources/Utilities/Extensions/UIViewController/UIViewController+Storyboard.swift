//
//  UIViewController+Storyboard.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19.02.18.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import UIKit

extension UIViewController {
    class func fromStoryboard<Type: UIViewController>() -> Type {
        let name = "\(Type.self)"
        let storyboard = UIStoryboard(name: name, bundle: nil)

        guard let viewController = storyboard.instantiateInitialViewController() as? Type else {
            fatalError("Failed to create viewController from storyboard \(name)")
        }
        
        return viewController
    }
}
