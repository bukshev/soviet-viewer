//
//  DashboardItemsManager.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19.02.18.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import Foundation

struct DashboardItemsManager {
    static func items(from posts: [Post]) -> [DashboardPostItem] {
        return posts.map { return DashboardPostItem(post: $0, placeholderImageName: "PlaceholderImage") }
    }

    static func emptyViewItem() -> SectionItem {
        return DashboardEmptyViewItem(imageName: "EmptyViewImage")
    }
}
