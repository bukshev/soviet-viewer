//
//  AppAssembler.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19.02.18.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import Swinject

enum AppAssembler {
    static let shared = Assembler([
        CoreServiceComponents(),
        LocalServicesAssembly(),
        SplashModuleAssembly(),
        DashboardModuleAssembly(),
        PhotoViewModuleAssembly()
        ])

    static func resolve<Service>(_ serviceType: Service.Type) -> Service? {
        return shared.resolver.resolve(serviceType)
    }
}
