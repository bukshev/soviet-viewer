//
//  AppDelegate.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19.02.18.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow()
        window?.rootViewController = UIViewController()
        window?.makeKeyAndVisible()

        let assembler = AppAssembler.shared
        guard let viewController = assembler.resolver.resolve(SplashViewController.self) else {
            fatalError("Can't resolve SplashViewController in AppDelegate")
        }

        window?.rootViewController = viewController

        return true
    }

}

