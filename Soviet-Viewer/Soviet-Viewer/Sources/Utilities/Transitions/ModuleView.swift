//
//  ModuleView.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19.02.18.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

private protocol ModuleInput: class {

}

protocol ModuleView {
    // Can't inherit from ModuleInput
    // because currently Swift don't allow use other protocols for output var
    associatedtype Output

    var output: Output! { get }
}
