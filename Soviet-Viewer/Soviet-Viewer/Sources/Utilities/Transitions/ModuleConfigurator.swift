//
//  ModuleConfigurator.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19.02.18.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import UIKit

protocol ModuleConfiguratorProtocol: ModuleTransitionProtocol {
    @discardableResult func configure<Input>(handler: (Input) -> Void) -> ModuleTransitionProtocol
}

struct ModuleConfigurator: ModuleConfiguratorProtocol {
    let source: UIViewController
    let destination: UIViewController
    let output: Any?

    @discardableResult
    func configure<Input>(handler: (Input) -> Void) -> ModuleTransitionProtocol {
        if let input = output as? Input {
            handler(input)
        }

        return ModuleTransition(source: source, destination: destination)
    }

    func transition(handler: (UIViewController, UIViewController) -> Void) {
        let transition = ModuleTransition(source: source, destination: destination)
        transition.transition(handler: handler)
    }
}
