//
//  PhotoViewModuleAssembly.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 20/02/2018.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import Swinject

class PhotoViewModuleAssembly: Assembly {
    typealias View = PhotoViewController
    typealias Presenter = PhotoViewPresenter
    typealias Router = PhotoViewRouter

    func assemble(container: Container) {
        container.register(ModuleFactory<View>.self) { r in
            return ModuleFactory {
                return r.resolve(View.self)!
            }
        }

        container.register(View.self) { r in
            let c: View = .fromStoryboard()
            c.output = r.resolve(Presenter.self)
            return c
        }

        container.register(Presenter.self) { r in
            let c = Presenter()
            c.router = r.resolve(Router.self)
            return c
        } .initCompleted { r, c in
            c.view = r.resolve(View.self)
        }

        container.register(Router.self) { r in
            let c = Router()
            return c
        } .initCompleted { r, c in
            c.transitionHandler = r.resolve(View.self)
        }
    }
}
