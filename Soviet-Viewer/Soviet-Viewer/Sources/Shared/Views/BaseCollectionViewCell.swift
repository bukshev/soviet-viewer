//
//  BaseCollectionViewCell.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19.02.18.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import UIKit

enum CellSizeType {
    case none
    case width
    case height
    case all
}

typealias CellLayoutAttributes = UICollectionViewLayoutAttributes

class BaseCollectionViewCell: UICollectionViewCell, SectionItemCell {
    var cellSize: CellSizeType = .height

    override func awakeFromNib() {
        super.awakeFromNib()

        updateCellSizeType()
        updateAppearance()
    }

    override func didMoveToWindow() {
        super.didMoveToWindow()

        updateAppearance()
    }

    func updateCellSizeType() {

    }

    func updateAppearance() {

    }

    func configure(with item: SectionItem) {
        
    }

    func deconfigure(with item: SectionItem) {

    }

    override func preferredLayoutAttributesFitting(_ layoutAttributes: CellLayoutAttributes) -> CellLayoutAttributes {
        return preferredLayoutAttributesFitting(layoutAttributes, type: cellSize)
    }

    func preferredLayoutAttributesFitting(_ layoutAttributes: CellLayoutAttributes, type: CellSizeType) -> CellLayoutAttributes {
        if type == .none {
            return layoutAttributes
        }

        frame = layoutAttributes.frame

        setNeedsLayout()
        layoutIfNeeded()

        let size = contentView.systemLayoutSizeFitting(layoutAttributes.frame.size)
        var newFrame = layoutAttributes.frame

        switch type {
        case .width:
            newFrame.size.width = ceil(size.width)
        case .height:
            newFrame.size.height = ceil(size.height)
        case .all:
            newFrame.size.width = ceil(size.width)
            newFrame.size.height = ceil(size.height)
        default:
            break
        }

        layoutAttributes.frame = newFrame
        return layoutAttributes
    }
}
