//
//  PhotoViewIO.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 20/02/2018.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import Foundation

protocol PhotoViewInput: class {
    func setupState(with post: Post?)
}

protocol PhotoViewOutput: class {
    func triggerViewReadyEvent()
    func triggerCloseEvent()

    func triggerShowPreviousImage()
    func triggerShowNextImage()
}
