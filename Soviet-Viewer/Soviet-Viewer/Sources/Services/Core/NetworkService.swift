//
//  NetworkService.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19.02.18.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import Alamofire

protocol NetworkServiceProtocol {
    func request<Type>(request: NetworkServiceRequest) -> Signal<Type>
}

class NetworkService {
    let manager: SessionManager
    let printResponses = false

    init(configuration: URLSessionConfiguration = .default) {
        self.manager = SessionManager(configuration: configuration)
    }
}

fileprivate extension NetworkService {
    func request(request: NetworkServiceRequest) -> DataRequest {
        let path = request.url.absoluteString
        let method = request.methodToAlamofire()
        let encoding = request.encodingToAlamofire()
        let parameters = request.parametersToAlamofire()
        let headers = request.headers
        return manager.request(path, method: method, parameters: parameters, encoding: encoding, headers: headers)
    }

    func isApiError(value: Any) -> SovietError? {
        if let dict = value as? JSONType,
            let embedded = dict["_embedded"] as? JSONType,
            let errorsArray = embedded["errors"] as? JSONArrayType,
            errorsArray.count > 0 {

            let dict = errorsArray[0]
            guard let message = dict["message"] as? String else { return nil }
            return .api(description: message, json: dict)
        } else if let dict = value as? JSONType, let error = dict["error"] as? String {
            return .api(description: error, json: dict)
        } else if let array = value as? [String] {
            return .validation(array)
        }
        return nil
    }

    func handleInvalidStatusCodeResponse<Type>(response: DataResponse<Any>, event: SignalEvent<Type>) {
        switch response.result {
        case let .success(value):
            if let error = isApiError(value: value) {
                event.failure(error: error)
            } else {
                event.failure(error: .api(description: "Unknown Error", json: nil))
            }
        case let .failure(error):
            event.failure(error: .network(description: error.localizedDescription))
        }
    }

    func handleResponse<Type>(response: DataResponse<Any>, event: SignalEvent<Type>) {
        switch response.result {
        case let .success(value):
            if let error = isApiError(value: value) {
                event.failure(error: error)
            } else if let value = value as? Type {
                event.success(value: value)
            } else {
                event.failure(error: .invalidType(expect: Type.self, got: type(of: value)))
            }
        case let .failure(error):
            handleErrorResponse(error: error, event: event)
        }
    }

    func handleErrorResponse<Type>(error: Error, event: SignalEvent<Type>) {
        switch error {
        case let AFError.responseSerializationFailed(reason: reason):
            switch reason {
            case .inputDataNil, .inputDataNilOrZeroLength:
                if let value = [:] as? Type {
                    event.success(value: value)
                } else {
                    event.failure(error: .network(description: error.localizedDescription))
                }
            default:
                event.failure(error: .network(description: error.localizedDescription))
            }
        case AFError.ResponseSerializationFailureReason.jsonSerializationFailed(error: _):
            if let value = [:] as? Type {
                event.success(value: value)
            } else {
                event.failure(error: .network(description: error.localizedDescription))
            }
        default:
            event.failure(error: .network(description: error.localizedDescription))
        }
    }
}

extension NetworkService: NetworkServiceProtocol {
    func request<Type>(request: NetworkServiceRequest) -> Signal<Type> {
        return Signal { event in
            self.request(request: request).responseJSON { response in
                let range = 200..<300
                guard let statusCode = response.response?.statusCode, range.contains(statusCode) else {
                    self.handleInvalidStatusCodeResponse(response: response, event: event)
                    return
                }
                self.handleResponse(response: response, event: event)
            }
        }
    }
}

fileprivate extension NetworkServiceRequest {
    func methodToAlamofire() -> Alamofire.HTTPMethod {
        switch method {
        case .get:
            return .get
        case .post:
            return .post
        case .put:
            return .put
        }
    }

    func encodingToAlamofire() -> Alamofire.ParameterEncoding {
        switch encoding {
        case .methodDependent:
            return URLEncoding.methodDependent
        case .queryString:
            return URLEncoding.queryString
        case .httpBody:
            return URLEncoding.httpBody
        case .JSON:
            if let parameters = parameters {
                switch parameters {
                case .array(_):
                    return NetworkServiceJSONArrayParameterEncoding()
                default:
                    return JSONEncoding.default
                }
            }
            return JSONEncoding.default
        }
    }

    func parametersToAlamofire() -> JSONType? {
        if let parameters = parameters {
            switch parameters {
            case let .dictionary(dictionary):
                return dictionary
            case let .array(array):
                return ["array": array]
            }
        }
        return nil
    }
}

fileprivate struct NetworkServiceJSONArrayParameterEncoding: Alamofire.ParameterEncoding {
    func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var urlRequest = try urlRequest.asURLRequest()

        guard let array = parameters?["array"], let data = try? JSONSerialization.data(withJSONObject: array, options: []) else {
            return urlRequest
        }

        if urlRequest.value(forHTTPHeaderField: "Content-Type") == nil {
            urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        }

        urlRequest.httpBody = data
        return urlRequest
    }
}
