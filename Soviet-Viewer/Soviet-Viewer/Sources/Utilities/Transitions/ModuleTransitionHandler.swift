//
//  ModuleTransitionHandler.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19.02.18.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import UIKit

protocol ModuleTransitionHandlerProtocol: class {
    func openModule<View>(factory: ModuleFactory<View>) -> ModuleConfiguratorProtocol
    func close(animated: Bool)
}

extension UIViewController: ModuleTransitionHandlerProtocol {
    func openModule<View>(factory: ModuleFactory<View>) -> ModuleConfiguratorProtocol {
        let view = factory.instantiateModuleTransitionHandler()
        return ModuleConfigurator(source: self, destination: view, output: view.output)
    }

    func close(animated: Bool) {
        if let _ = presentingViewController {
            dismiss(animated: animated, completion: nil)
        } else if let navigationController = navigationController, navigationController.viewControllers.count > 0 {
            navigationController.popViewController(animated: animated)
        } else if let _ = view.superview {
            view.removeFromSuperview()
        }
    }

    func viewController() -> UIViewController {
        return self
    }
}

