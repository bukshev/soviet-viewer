//
//  ModuleFactory.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19.02.18.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import UIKit

struct ModuleFactory<View: ModuleView> where View: UIViewController {
    let handler: () -> View

    init(handler: @escaping () -> View) {
        self.handler = handler
    }

    func instantiateModuleTransitionHandler() -> View {
        return handler()
    }
}
