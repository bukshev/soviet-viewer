//
//  DashboardInteractor.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 20.02.18.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import Foundation

class DashboardInteractor {
    weak var output: DashboardInteractorOutput!

    var reachabilityService: ReachabilityServiceProtocol!
    var postsService: PostsServiceProtocol!
    var cacheService: CacheServiceProtocol!
}

extension DashboardInteractor: DashboardInteractorInput {
    func loadNewPosts(page: Int) {
        guard reachabilityService.hasInternerConnection else {
            output.didFailureLoadNewPosts(error: .network(description: "Bad Internet connection"))
            return
        }

        postsService.loadPosts(hashtag: "chow-chow", page: page) { (result) in
            switch result {
            case let .success(posts):
                self.cacheService.cache(objects: posts)
                self.output.didLoad(newPosts: posts)
            case let .failure(error):
                self.output.didFailureLoadNewPosts(error: error)
            }
        }
    }
}
