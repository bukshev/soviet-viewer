//
//  DisplayDirectorCellCache.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19.02.18.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import UIKit

class DisplayDirectorCellCache {
    static let shared = DisplayDirectorCellCache()

    var cache = [String: UICollectionViewCell]()

    func cellKey(for item: SectionItem) -> String? {
        switch item.cellIdentifier {
        case let .cellType(CellType):
            return "\(CellType)_type"
        case let .nibName(name):
            return "\(name)_nib"
        default:
            return nil
        }
    }

    func cell(for item: SectionItem) -> UICollectionViewCell? {
        guard let key = cellKey(for: item) else { return nil }
        if let cell = cache[key] { return cell }

        switch item.cellIdentifier {
        case let .cellType(CellType):
            let cell = CellType.init()
            cache[key] = cell
            return cell
        case let .nibName(name):
            let views = UINib(nibName: name, bundle: nil).instantiate(withOwner: nil, options: nil)
            if let cell = views.first as? UICollectionViewCell {
                cache[key] = cell
                return cell
            }
        default:
            break
        }
        return nil
    }
}
