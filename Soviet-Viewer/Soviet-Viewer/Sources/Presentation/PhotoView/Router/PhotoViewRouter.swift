//
//  PhotoViewRouter.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 20/02/2018.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import Foundation

class PhotoViewRouter {
    weak var transitionHandler: ModuleTransitionHandlerProtocol!
}

extension PhotoViewRouter: PhotoViewRouterInput {
    func close() {
        transitionHandler.close(animated: true)
    }
}
