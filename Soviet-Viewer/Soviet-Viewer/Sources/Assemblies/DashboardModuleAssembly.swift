//
//  DashboardModuleAssembly.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19/02/2018.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import Swinject

class DashboardModuleAssembly: Assembly {
    typealias View = DashboardViewController
    typealias Presenter = DashboardPresenter
    typealias Interactor = DashboardInteractor
    typealias Router = DashboardRouter

    func assemble(container: Container) {
        container.register(ModuleFactory<View>.self) { r in
            return ModuleFactory {
                return r.resolve(View.self)!
            }
        }

        container.register(View.self) { r in
            let c: View = .fromStoryboard()
            c.output = r.resolve(Presenter.self)
            return c
        }

        container.register(Presenter.self) { r in
            let c = Presenter()
            c.interactor = r.resolve(Interactor.self)
            c.router = r.resolve(Router.self)
            return c
        } .initCompleted { r, c in
            c.view = r.resolve(View.self)
        }

        container.register(Interactor.self) { r in
            let c = Interactor()
            c.reachabilityService = r.resolve(ReachabilityServiceProtocol.self)
            c.postsService = r.resolve(PostsServiceProtocol.self)
            c.cacheService = r.resolve(CacheServiceProtocol.self)
            return c
            } .initCompleted { r, c in
                c.output = r.resolve(Presenter.self)
        }

        container.register(Router.self) { r in
            let c = Router()
            c.photoViewModuleFactory = r.resolve(ModuleFactory<PhotoViewController>.self)
            return c
        } .initCompleted { r, c in
            c.transitionHandler = r.resolve(View.self)
        }
    }
}
