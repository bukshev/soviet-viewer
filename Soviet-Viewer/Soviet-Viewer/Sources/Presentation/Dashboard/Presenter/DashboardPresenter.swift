//
//  DashboardPresenter.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19/02/2018.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import Foundation

class DashboardPresenter {
    weak var view: DashboardViewInput!
    var interactor: DashboardInteractorInput!
    var router: DashboardRouterInput!
    weak var displayDirector: DisplayDirector!
    var loading: Bool = false

    fileprivate var posts = [Post]()

    fileprivate let chunkSize = 20
    fileprivate var currentChunk = 1
    fileprivate var currentPage = 1
    fileprivate var chunksItems = [DashboardPostItem]()
}

extension DashboardPresenter: DashboardModuleInput {
    func set(posts: [Post]) {
        self.posts = posts
    }
}

extension DashboardPresenter: DashboardViewOutput {
    func triggerViewReadyEvent() {
        view.setupInitialState()
        configureView()
    }

    func triggerLoadMoreEvent() {
        loading = true
        loadNextChunk()
    }
}

extension DashboardPresenter: DashboardInteractorOutput {
    func didLoad(newPosts: [Post]) {
        loading = false
        posts.append(contentsOf: newPosts)
        addChunk()
    }

    func didFailureLoadNewPosts(error: SovietError) {
        loading = false
        currentPage -= 1
    }
}

extension DashboardPresenter: PhotoViewModuleOutput {
    func post(before index: Int) -> Post? {
        return (index - 1 >= 0) ? posts[index-1] : nil
    }

    func post(after index: Int) -> Post? {
        guard index + 1 < posts.count else {
            loadNextChunk()
            return nil
        }
        return posts[index+1]
    }
}

fileprivate extension DashboardPresenter {
    func configureView() {
        guard posts.count > 0 else {
            let item = DashboardItemsManager.emptyViewItem()
            let source = DisplayDirectorSource(data: [item], type: SectionController.self)
            displayDirector.setSources([source])
            return
        }
        let items = DashboardItemsManager.items(from: Array(posts[0..<chunkSize]))
        configure(items: items)
        chunksItems.append(contentsOf: items)
        displayDirector.setSources([DisplayDirectorSource(data: chunksItems, type: SectionController.self)])
    }

    func loadNextChunk() {
        DispatchQueue.global(qos: .default).async {
            // Add chunk to tail if we can, else load new posts
            if self.chunkSize * (self.currentChunk + 1) < self.posts.count {
                self.addChunk()
            } else {
                self.loadNewPage()
            }
        }
    }

    func addChunk() {
        guard ((currentChunk+1) * chunkSize) < posts.count else { return }

        currentChunk += 1
        let nextChunk = Array(posts[chunksItems.count..<(currentChunk * chunkSize)])

        // Configure chunk for displaying
        let items = DashboardItemsManager.items(from: nextChunk)
        configure(items: items)

        // Add chunk to tail
        DispatchQueue.main.async {
            self.loading = false
            self.chunksItems.append(contentsOf: items)
            self.displayDirector.setSources([DisplayDirectorSource(data: self.chunksItems, type: SectionController.self)])
        }
    }

    func loadNewPage() {
        currentPage += 1
        interactor.loadNewPosts(page: currentPage)
    }

    func configure(items: [DashboardPostItem]) {
        for item in items {
            item.action = { (_, _) in
                self.handleTap(post: item.post)
            }
        }
    }

    func handleTap(post: Post) {
        guard let index = posts.index(where: { $0 == post }) else { return }
        router.openPhotoView(post: post, index: index, moduleOutput: self)
    }
}
