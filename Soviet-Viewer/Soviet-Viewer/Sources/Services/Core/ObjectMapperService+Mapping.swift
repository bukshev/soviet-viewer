//
//  ObjectMapperService+Mapping.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 20.02.18.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import ObjectMapper

extension Map {
    func missingRequired(keys: String...) -> Bool {
        for key in keys {
            if JSON[key] == nil {
                return true
            }
        }
        return false
    }
}

extension Post: MappableProtocol {
    convenience init?(map: Map) {
        self.init()
        guard !map.missingRequired(keys: "id", "farm") else { return nil }

        self.identifier = ""
        self.farm = 5
        self.secret = ""
        self.server = ""
        self.title = ""
        self.imageData = nil
    }

    func mapping(map: Map) {
        identifier <- map["id"]
        farm <- map["farm"]
        secret <- map["secret"]
        server <- map["server"]
        title <- map["title"]
    }
}
