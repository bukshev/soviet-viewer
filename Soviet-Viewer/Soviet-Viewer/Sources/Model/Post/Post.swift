//
//  Post.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19.02.18.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import Foundation
import RealmSwift

final class Post: Object {
    @objc dynamic var identifier = ""
    @objc dynamic var farm = 1
    @objc dynamic var secret = ""
    @objc dynamic var server = ""
    @objc dynamic var title = ""

    @objc dynamic var imageData: Data?
}

extension Post {
    func imageURL() -> URL? {
        return FlickrApi.imageUrl(post: self)
    }

    func update(imageData: Data) {
        do {
            let realm = try Realm()
            realm.beginWrite()
            self.imageData = imageData
            try realm.commitWrite()
        } catch {
            debugPrint("Can't write image data to Realm")
        }
    }
}
