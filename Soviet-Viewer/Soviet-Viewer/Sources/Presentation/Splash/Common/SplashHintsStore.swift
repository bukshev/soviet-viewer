//
//  SplashHintsStore.swift
//  Soviet-Viewer
//
//  Created by Ivan Bukshev on 19.02.18.
//  Copyright © 2018 Team Absurdum. All rights reserved.
//

import Foundation

struct SplashHintsStore {
    private let hints = [
        "Целились в коммунизм, а попали в Россию.",
        "Призрак бродит по Европе, призрак коммунизма.",
        "Коммунизм - когда вооружены до зубов, а зубов нет.",
        "Коммунизм - как сухой закон: идея хорошая, но не работает.",
        " Коммунизм - это скачки, где все получают первое место, но без приза.",
        "— Является ли коммунизм наукой?\n— Нет. Если бы был наукой, то сначала попробовали на собаках.",
        "— Будет ли воровство при коммунизме?\n— Нет, все разворуют при социализме."
    ]

    func randomHint() -> String {
        let randomIndex = Int(arc4random_uniform(UInt32(hints.count)))
        return hints[randomIndex]
    }
}
